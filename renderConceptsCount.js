const _ = require('lodash')
const conceptsCount = require('lo-concepts/templates/conceptsCount.js')
const htmlToElement = require('lo-concepts/htmlToElement.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    const html = conceptsCount({
        count: _.size(args.instance.props.concepts)
    })

    const element = htmlToElement(html)
    args.instance.props.conceptsCountTarget.appendChild(element)
    args.instance.props.elements.conceptsCount.push(element)

    args.instance.events.emit('countRender', {
        element: element
    })
    
    return element
}