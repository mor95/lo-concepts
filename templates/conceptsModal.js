(function(){function conceptsModal(it
/**/) {
var out='<div class="concepts-modal"> <div class="close-button hoverable"> <i class="fa fa-times"></i> </div> <h1 class="animated bounceIn">Conceptos Obtenidos</h1> <div class="concepts animated bounce"> '; for(var prop in it.concepts) { out+=' <div class="concept"> <div class="star"></div> <span class="concept-name">'+(prop)+':</span> <span class="concept-description">'+(it.concepts[prop])+'</span> </div> '; } out+=' </div></div>';return out;
}var itself=conceptsModal, _encodeHTML=(function (doNotSkipEncoded) {
		var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': "&#34;", "'": "&#39;", "/": "&#47;" },
			matchHTML = doNotSkipEncoded ? /[&<>"'\/]/g : /&(?!#?\w+;)|<|>|"|'|\//g;
		return function(code) {
			return code ? code.toString().replace(matchHTML, function(m) {return encodeHTMLRules[m] || m;}) : "";
		};
	}());if(typeof module!=='undefined' && module.exports) module.exports=itself;else if(typeof define==='function')define(function(){return itself;});else {window.render=window.render||{};window.render['conceptsModal']=itself;}}());