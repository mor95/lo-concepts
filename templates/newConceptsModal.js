(function(){function newConceptsModal(it
/**/) {
var out='<div class="content"> <iframe src="img/brain-animate/brain.html" scrolling="no" class="brain-icon animated pulse" frameborder="0"></iframe> <div class="concepts"> '; for(var prop in it.concepts) { out+=' <div class="concept"> <div class="star"></div> <span class="concept-name">'+(prop)+'</span> <!-- <span class="concept-description">'+(it.concepts[prop])+'</span> --> </div> '; } out+=' </div> <h1>'+(it.title)+'</h1> <div class="challenge-title animated rubberBand"> <h3>RETO MENTAL </h3> <h2>'+(it.subtitle)+'</h2> </div></div>';return out;
}var itself=newConceptsModal, _encodeHTML=(function (doNotSkipEncoded) {
		var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': "&#34;", "'": "&#39;", "/": "&#47;" },
			matchHTML = doNotSkipEncoded ? /[&<>"'\/]/g : /&(?!#?\w+;)|<|>|"|'|\//g;
		return function(code) {
			return code ? code.toString().replace(matchHTML, function(m) {return encodeHTMLRules[m] || m;}) : "";
		};
	}());if(typeof module!=='undefined' && module.exports) module.exports=itself;else if(typeof define==='function')define(function(){return itself;});else {window.render=window.render||{};window.render['newConceptsModal']=itself;}}());