const _ = require('lodash')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.concepts))
        throw new Error('Invalid concepts')

    if(!_.isObject(args.instance))
        throw new Error('Invalid loConcepts instance')

    if(!_.isObject(args.APIInstance))
        throw new Error('Invalid APIInstance')

    if(!_.isObject(args.courseData))
        throw new Error('A courseData object property is expected.')

    return args.APIInstance.makeRequest({
        component: 'concepts',
        method: 'addLOConcepts',
        arguments: {
            shortName: args.courseData.shortName,
            unit: args.courseData.unit,
            concepts: _.keys(args.concepts)
        }
    })
    .then(function (response) {
        if (response !== true) {
            swal({
                type: 'error',
                title: 'Error en la respuesta del servidor.',
                text: 'Ocurrió un error al agregar los conceptos'
            })
            console.error(response)
            throw new Error('Expected a true response.')
        }

        _.forEach(args.concepts, function (v, k) {
            args.instance.props.concepts[k] = v
        })

        args.instance.refreshConceptsCount()
    })
}