const _ = require('lodash')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.APIInstance))
        throw new Error('Invalid APIInstance')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    return args.APIInstance.makeRequest({
        component: 'concepts',
        method: 'deleteLOConcepts',
        arguments: {
            shortName: args.courseShortName,
            unit: args.courseUnit,
            concepts: _.keys(args.concepts)
        }
    })
    .then(function (response) {
        if (response !== true) {
            swal({
                type: 'error',
                title: 'Error en la respuesta del servidor.',
                text: 'Ocurrió un error al eliminar los conceptos asociados a este usuario'
            })
            console.error(response)
            throw new Error('Expected a true response.')
        }

        _.forEach(args.concepts, function (v, k) {
            delete args.instance.props.concepts[k]
        })
        
        args.instance.refreshConceptsCount()
    })
}